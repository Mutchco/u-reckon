# You Reckon?

#### A embedded shopify app that let's shop owners add a simple guessing game to their product page
##### Scaffolded with the shopify cli

## Installation

```sh
  npm install
```

## Requirements

- If you don’t have one, [create a Shopify partner account](https://partners.shopify.com/signup).
- If you don’t have one, [create a Development store](https://help.shopify.com/en/partners/dashboard/development-stores#create-a-development-store) where you can install and test your app.
- In the Partner dashboard, [create a new app](https://help.shopify.com/en/api/tools/partner-dashboard/your-apps#create-a-new-app). You’ll need this app’s API credentials during the setup process.

## Usage

```sh
  npm run dev
```

## Files

### /pages directory

This is where the main pages for the embedded app are

##### /pages/_app

Main app component

##### /pages/index

Main index page, renders a welcome screen or products list

##### /pages/settings

Main settings page, edits shop privateMetafields

##### /pages/edit-products

Edit product page, this is where you can set the secret code, max tries and discount

### /components directory

This is where the embedded app components are

##### /components/ClientRouter

Main router

##### /components/ResourceList

A list component that displays products and the associated meta fields

##### /components/SettingsForm

A form component for saving shop privateMetafields

### /assets directory

This is where the store facing assets are

##### /assets/ureckon.js

This is the frontend javascript for the store

### /theme-assets directory

This is where all .liquid assets are

##### /theme-assets/ureckon-loader

This is a piece of .liquid code that needs to be added to the store's theme

##### /theme-assets/ureckon-snippet

This is a snippet that loads the main guessing game, it's added to a theme's snippets on installation