(function () {
  // Main button
  const ctaButton = document.querySelector('#ureckon-cta');

  if (!ctaButton) {
    console.error('ureckon error: did you add the snippet loader?');
    return;
  }

  // Error and warning messages
  const messages = {
    guessInvalid: 'Please enter a valid guess',
    guessIsNaN: 'Please enter a number',
    guessIsToHigh: 'Try a lower number',
    guessIsToLow: 'Try a higher number',
  };

  // Style classes for messages
  const styleClasses = {
    fail: 'ureckon-fail',
    warning: 'ureckon-warning',
    success: 'ureckon-success',
  };

  // Hidden inputs
  const productHandleInput = document.querySelector('#ureckon-product-handle');

  // UI Elements
  const formWrapper = document.querySelector('#ureckon-guess-form');
  const guessInput = document.querySelector('#ureckon-guess-input');
  const guessButton = document.querySelector('#ureckon-guess-submit');
  const messageBox = document.querySelector('#ureckon-message-box');
  const guessLeftBox = document.querySelector('#ureckon-guesses-left');

  // Main request handler for init and guessins
  const sendGuessRequest = (guess) => {
    guessLeftBox.innerHTML = '';
    // This should be a variable injected by a build process
    fetch('https://fe271a2443f3.ngrok.io/ureckon/guess', {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'post',
      body: JSON.stringify({
        shop: Shopify.shop,
        productHandle: productHandleInput.value,
        token: localStorage.getItem('ureckon-token'),
        guess,
      }),
    })
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        const {
          token,
          success,
          message,
          data: {
            isDisabled,
            isCorrect,
            isHigher,
            guessesLeft,
            discountCode,
            productId,
          },
        } = data;

        if (isDisabled === true) {
          return;
        } else {
          ctaButton.setAttribute('style', 'display: inline-block');
        }

        // If we're not successful, hide the guess button
        if (!success) {
          messageBox.textContent = message;
          messageBox.classList.remove(styleClasses.success, styleClasses.warning);
          messageBox.classList.add(styleClasses.fail);
          guessButton.setAttribute('style', 'display: none');
          return;
        }

        // Store token in local storage
        localStorage.setItem('ureckon-token', token);

        if (!isCorrect && guess !== undefined) {
          messageBox.classList.add(styleClasses.warning);
          messageBox.textContent = isHigher
            ? messages.guessIsToHigh
            : messages.guessIsToLow;
          const b = document.createElement('b');
          b.textContent = guessesLeft;
          guessLeftBox.innerHTML = `You have ${b.outerHTML} guesses remaining`;
          guessButton.removeAttribute('disabled');
        } else if (isCorrect === true) {
          messageBox.classList.remove(styleClasses.fail, styleClasses.warning);
          messageBox.classList.add(styleClasses.success);
          messageBox.textContent = 'You guessed correctly!';
          const id = productId.replace('gid://shopify/ProductVariant/', '');
          // Add the product variant to the cart
          fetch('/cart/add.js', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              items: [
                {
                  quantity: 1,
                  id,
                }
              ]
            }),
          })
            .then(function (response) {
              return response.json();
            })
            .then(function (data) {
              // Redirect to the checkout with the code added
              window.location.href = `/discount/${discountCode}?redirect=/cart/checkout`;
            })
            .catch(function (error) {
              console.error(error);
            })
        }
      })
      .catch(function (error) {
        console.error('ureckon error:', error);
        messageBox.textContent = 'An error occurred with our backend, sorry!';
        guessButton.removeAttribute('disabled');
      });
  };

  sendGuessRequest();

  // Opens guessing form wrapper
  ctaButton.addEventListener('click', function (event) {
    if (event.stopPropagation) {
      event.stopPropagation();
    }

    if (event.preventDefault) {
      event.preventDefault();
    }

    formWrapper.setAttribute('style', 'display:flex');
  });

  // Basic validation on submit
  guessButton.addEventListener('click', function () {
    guessButton.setAttribute('disabled', 'true');
    messageBox.textContent = '';
    const currentGuess = guessInput.value;

    if (currentGuess === '') {
      messageBox.textContent = messages.guessInvalid;
      guessButton.removeAttribute('disabled');
      return;
    }

    if (isNaN(parseFloat(currentGuess))) {
      messageBox.textContent = messages.guessIsNaN;
      guessButton.removeAttribute('disabled');
      return;
    }

    sendGuessRequest(currentGuess);
  });
})();