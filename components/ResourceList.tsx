import React from 'react';
import gql from 'graphql-tag';
import { Query, QueryResult, useQuery } from 'react-apollo';
import {
  Card,
  ResourceList,
  SettingToggle,
  Stack,
  TextStyle,
  Thumbnail,
} from '@shopify/polaris';
import store from 'store-js';
import { Redirect } from '@shopify/app-bridge/actions';
import { Context, useAppBridge } from '@shopify/app-bridge-react';

export default function ResourceListWithProducts({ items }) {
  const app = useAppBridge();
  // Redirect to u reckon details onclick
  const redirectToProduct = () => {
    const redirect = Redirect.create(app);
    redirect.dispatch(
      Redirect.Action.APP,
      '/edit-products',
    );
  };
  return (
    <Card>
      <ResourceList
        showHeader
        resourceName={{ singular: 'Product', plural: 'Products' }}
        items={items}
        renderItem={({ node: item }) => {
          const media = (
            <Thumbnail
              source={
                item.images.edges[0]
                  ? item.images.edges[0].node.originalSrc
                  : ''
              }
              alt={
                item.images.edges[0]
                  ? item.images.edges[0].node.altText
                  : ''
              }
            />
          );

          const discount = item.privateMetafields.edges[0]?.node.value;
          const maxTries = item.privateMetafields.edges[2]?.node.value;
          const enabled = item.privateMetafields.edges[3]?.node.value;

          return (
            <ResourceList.Item
              id={item.id}
              media={media}
              accessibilityLabel={`View details for ${item.title}`}
              onClick={() => {
                store.set('u-reckon-product', item);
                redirectToProduct();
              }}
            >
              <Stack alignment="center">
                <Stack.Item fill>
                  <h3>
                    <TextStyle variation="strong">
                      {item.title}
                    </TextStyle>
                  </h3>
                </Stack.Item>
                <Stack.Item>
                  <p>{!!discount ? `${discount}% Off` : ''}</p>
                </Stack.Item>
                <Stack.Item>
                  <p>{!!maxTries ? `${maxTries} Tries` : ''}</p>
                </Stack.Item>
                <Stack.Item>
                  <p>{enabled === 'enabled' ? 'Enabled' : 'Not Enabled'}</p>
                </Stack.Item>
              </Stack>
            </ResourceList.Item>
          );
        }}
      />
    </Card>
  );
}
