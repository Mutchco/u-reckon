import React, { useCallback, useState } from 'react';
import {
  Button,
  Card,
  Form,
  FormLayout,
  Frame,
  InlineError,
  Layout,
  Page,
  SettingToggle,
  Stack,
  TextField,
  TopBar,
  ContextualSaveBar,
  Toast ,
} from '@shopify/polaris';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { ENABLED_FOR_SHOP, MAX_DISCOUNT_CODES, META_NAMESPACE } from '../server/constants';
import gql from 'graphql-tag';
import { Loading } from '@shopify/app-bridge-react';

// Mutation for updating shop privateMetafields
const SHOP_METAFIELD_MUTATION = gql`
mutation privateMetafieldUpsert($input: PrivateMetafieldInput!) {
  privateMetafieldUpsert(input: $input) {
    privateMetafield {
      id
      key
      value
    }
    userErrors {
      field
      message
    }
  }
}
`;

export default function SettingsForm({ shop }) {
  const [updateMeta, { loading, error, data }] = useMutation(SHOP_METAFIELD_MUTATION, {
    onCompleted() {
      setToastActive(true);
      setIsDirty(false);
    },
  });

  const [isDirty, setIsDirty] = useState(false);
  const [toastActive, setToastActive] = useState(false);

  // Initial state
  const intialIsEnabled = shop.privateMetafields.edges[0]?.node.value;
  const initialMaxDiscounts = shop.privateMetafields.edges[1]?.node.value;

  // State
  const [isEnabled, setEnabled] = useState(intialIsEnabled || 'enabled');
  const [maxDiscounts, setMaxDiscounts] = useState(initialMaxDiscounts || '1');

  // State for error messages
  const [isEnabledError, setIsEnabledError] = useState('');
  const [maxDiscountsError, setMaxDiscountsError] = useState('');

  const handleEnabledChange = useCallback(
    () => {
      setEnabled(enabled => enabled !== 'disabled' ? 'disabled' : 'enabled');
      setIsDirty(true);
    },
    [],
  );

  const handleMaxDiscountsChange = useCallback(
    (value) => {
      setMaxDiscounts(value);
      setIsDirty(true);
    },
    [],
  );

  const toggleToastActive = useCallback(
    () => setToastActive(active => !active),
    [],
  );

  const handleClear = useCallback(
    () => {
      setEnabled(intialIsEnabled);
      setMaxDiscounts(initialMaxDiscounts.toString());
      setIsEnabledError('');
      setMaxDiscounts('');
      setIsDirty(false);
    },
    [],
  );

  const handleSubmit = useCallback(
    () => {
      updateMeta({
        variables: {
          input: {
            namespace: META_NAMESPACE,
            key: ENABLED_FOR_SHOP,
            valueInput: {
              value: isEnabled,
              valueType: 'STRING',
            },
          },
        },
      });

      updateMeta({
        variables: {
          input: {
            namespace: META_NAMESPACE,
            key: MAX_DISCOUNT_CODES,
            valueInput: {
              value: maxDiscounts,
              valueType: 'INTEGER',
            },
          },
        },
      });
    },
    [isEnabled, maxDiscounts],
  );

  const isEnabledText = isEnabled === 'enabled' ? 'Disable' : 'Enable';

  const toastMarkup = toastActive ? (
    <Toast content="Settings saved!" onDismiss={toggleToastActive} />
  ) : null;

  const loadingMarkup = loading ? <Loading /> : null;

  // Pop down save bar
  const contextualSaveBarMarkup = isDirty ? (
    <ContextualSaveBar
      message="Unsaved changes"
      saveAction={{
        loading,
        onAction: handleSubmit,
      }}
      discardAction={{
        onAction: handleClear,
      }}
    />
  ) : null;

  // If gql returns an error show this message
  const apiError = error && !isDirty ? (
    <InlineError message={'An error occurred while saving the settings'} fieldID="none" />
  ) : null;

  return (
    <Frame topBar={<TopBar />}>
      {loadingMarkup}
      {contextualSaveBarMarkup}
      <Page title="You Reckon? - Settings">
        {apiError}
        <Layout>
          <Layout.AnnotatedSection
            title="Max discounts"
            description="This is the maximum number of discount codes one customer can receive store wide."
          >
            <Card sectioned>
              <FormLayout>
                <TextField
                  value={maxDiscounts}
                  onChange={handleMaxDiscountsChange}
                  label="Max discounts per customer"
                  placeholder="2"
                />
              </FormLayout>
            </Card>
          </Layout.AnnotatedSection>
          <Layout.AnnotatedSection
            title="Disable game"
            description="Disable guessing game for all products"
          >
            <SettingToggle
              action={{
                content: isEnabledText,
                onAction: handleEnabledChange,
              }}
              enabled={isEnabled}
            >
              Globally enable or disable You Reckon?
        </SettingToggle>
          </Layout.AnnotatedSection>
        </Layout>
      </Page>
    </Frame>
  );
}
