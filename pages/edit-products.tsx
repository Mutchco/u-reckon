import React, { useCallback, useState } from 'react';
import {
  Banner,
  Button,
  ButtonGroup,
  Card,
  Form,
  FormLayout,
  Frame,
  InlineError,
  Layout,
  Page,
  SettingToggle,
  TextField,
  Toast,
  TopBar,
  ContextualSaveBar,
} from '@shopify/polaris';
import store from 'store-js';
import gql from 'graphql-tag';
import { useMutation } from 'react-apollo';
import { Loading, useAppBridge } from '@shopify/app-bridge-react';
import { Redirect } from '@shopify/app-bridge/actions';
import {
  DISCOUNT,
  ENABLED_FOR_PRODUCT,
  MAX_TRIES,
  META_NAMESPACE,
  PRICE_RULE_ID,
  SECRET_CODE,
} from '../server/constants';

// Gql query to update private meta fields
const UPDATE_METADATA = gql`
  mutation ($input: ProductInput!){
    productUpdate(input: $input) {
      product {
        privateMetafields(first:10) {
          edges {
            node {
              id
              namespace
              key
              value
            }
          }
        }
      }
      userErrors {
        field
        message
      }
    }
  }
`;

const ADD_PRICE_RULE = gql`
  mutation priceRuleCreate($input: PriceRuleInput!) {
    priceRuleCreate(priceRule: $input) {
      priceRule {
        id
      }
      priceRuleUserErrors {
        code
        field
        message
      }
    }
  }
`;

export default function EditProduct() {
  const app = useAppBridge();
  const product = store.get('u-reckon-product');

  const [isDirty, setIsDirty] = useState(false);
  const [toastActive, setToastActive] = useState(false);

  // Grab current values from the store product
  const initialDiscount = product.privateMetafields.edges[0]?.node.value;
  const initialSecretCode = product.privateMetafields.edges[1]?.node.value;
  const initialMaxTries = product.privateMetafields.edges[2]?.node.value;
  const initialIsEnabled = product.privateMetafields.edges[3]?.node.value;
  const initialPriceRuleId = product.privateMetafields.edges[4]?.node.value;

  const [secretCode, setSecretCode] = useState(initialSecretCode || '');
  const [discount, setDiscount] = useState(initialDiscount || '');
  const [maxTries, setMaxTries] = useState(initialMaxTries || '');
  const [isEnabled, setIsEnabled] = useState(initialIsEnabled || 'disabled');
  const [priceRuleId, setPriceRuleId] = useState(initialPriceRuleId || '');

  // State for error messages
  const [secretCodeError, setSecretCodeError] = useState('');
  const [discountError, setDiscountError] = useState('');
  const [maxTriesError, setMaxTriesError] = useState('');

  // Change handlers
  const handleSecretCodeChange = useCallback(
    (value) => {
      setSecretCode(value);
      value && setIsDirty(true);
    },
    [],
  );

  const handleDiscountChange = useCallback(
    (value) => {
      setDiscount(value);
      value && setIsDirty(true);
    },
    [],
  );

  const handleMaxTriesChange = useCallback(
    (value) => {
      setMaxTries(value);
      value && setIsDirty(true);
    },
    [],
  );

  const handleIsEnabledChange = useCallback(
    () => {
      setIsEnabled(val => val === 'enabled' ? 'disabled' : 'enabled');
      setIsDirty(true);
    },
    [],
  );

  const handleGenerate = useCallback(
    () => {
      const code = Math.round(Math.random() * 10000);
      setSecretCode(code.toString());
      setIsDirty(true);
    },
    [],
  );

  const [addPriceRule, priceRuleMutation] = useMutation(ADD_PRICE_RULE, {
    onCompleted() {
      updateProduct();
    },
    variables: {
      input: {
        target: 'LINE_ITEM',
        allocationMethod: 'EACH',
        title: `You Reckon ${product.handle}`,
        oncePerCustomer: true,
        customerSelection: {
          forAllCustomers: true,
        },
        itemEntitlements: {
          productIds: [
            product.id,
          ],
          targetAllLineItems: false,
        },
        prerequisiteQuantityRange: {
          greaterThanOrEqualTo: 1,
        },
        validityPeriod: {
          start: (new Date()).toISOString(),
        },
        value: {
          percentageValue: -(parseFloat(discount)),
        },
      },
    },
  });

  // Update call for product/private meta fiels
  // Probably a more elegant way than window.history.back
  const [updateProduct, productMutation] = useMutation(UPDATE_METADATA, {
    onCompleted() {
      setIsDirty(false);
      setToastActive(true);
    },
    variables: {
      input: {
        id: product.id,
        privateMetafields: [
          {
            namespace: META_NAMESPACE,
            key: DISCOUNT,
            valueInput: {
              value: discount,
              valueType: 'INTEGER',
            },
          }, {
            namespace: META_NAMESPACE,
            key: SECRET_CODE,
            valueInput: {
              value: secretCode,
              valueType: 'INTEGER',
            },
          }, {
            namespace: META_NAMESPACE,
            key: MAX_TRIES,
            valueInput: {
              value: maxTries,
              valueType: 'INTEGER',
            },
          }, {
            namespace: META_NAMESPACE,
            key: ENABLED_FOR_PRODUCT,
            valueInput: {
              value: isEnabled,
              valueType: 'STRING',
            },
          }, {
            namespace: META_NAMESPACE,
            key: PRICE_RULE_ID,
            valueInput: {
              value: priceRuleMutation.data?.priceRuleCreate?.priceRule.id,
              valueType: 'STRING',
            },
          },
        ],
      },
    },
  });

  const toggleToastActive = useCallback(
    () => setToastActive(active => !active),
    [],
  );

  const handleClear = useCallback(
    () => {
      setDiscount(initialDiscount);
      setSecretCode(initialSecretCode);
      setMaxTries(initialMaxTries);
      setIsEnabled(initialIsEnabled);
      setDiscountError('');
      setSecretCodeError('');
      setMaxTriesError('');
      setIsDirty(false);
    },
    [],
  );

  // Validation and submit handler
  // I might use something like Formik and yup.js here
  // Or even jsonschema and isomorphic validation
  const handleSubmit = useCallback(
    () => {
      setDiscountError('');
      setSecretCodeError('');
      setMaxTriesError('');
      let hasError = false;
      if (secretCode === '') {
        setSecretCodeError('Secret code is required');
        hasError = true;
      }

      if (discount === '') {
        setDiscountError('Discount is required');
        hasError = true;
      }

      if (maxTries === '') {
        setMaxTriesError('Max tries is required');
        hasError = true;
      }

      const discountParsed = parseFloat(discount);
      const maxTriesParsed = parseFloat(maxTries);
      const secretCodeParsed = parseFloat(secretCode);

      if (isNaN(discountParsed)) {
        setDiscountError('Discount must be a number');
        hasError = true;
      }

      if (isNaN(maxTriesParsed)) {
        setMaxTriesError('Max tries must be a number');
        hasError = true;
      }

      if (isNaN(secretCodeParsed)) {
        setSecretCodeError('Secret code must be a number');
        hasError = true;
      }

      if (!hasError) {
        addPriceRule();
      }

    },
    [secretCode, discount, maxTries],
  );

  const isEnabledText = isEnabled === 'enabled' ? 'Disable' : 'Enable';

  const toastMarkup = toastActive ? (
    <Toast content="Product saved!" onDismiss={toggleToastActive} />
  ) : null;

  const loading = priceRuleMutation.loading || productMutation.loading;
  const loadingMarkup = loading ? <Loading /> : null;

  // Pop down save bar
  const contextualSaveBarMarkup = isDirty ? (
    <ContextualSaveBar
      message="Unsaved changes"

      saveAction={{
        loading,
        onAction: handleSubmit,
      }}
      discardAction={{
        onAction: handleClear,
      }}
    />
  ) : null;

  const error = priceRuleMutation.error || productMutation.error;
  // If gql returns an error show this message
  const apiError = error && !isDirty ? (
    <InlineError message="An error occurred while saving the product" fieldID="none" />
  ) : null;

  return (
    <Frame
      topBar={<TopBar />}
    >
      {loadingMarkup}
      {contextualSaveBarMarkup}
      <Page
        title={`You Reckon? - '${product.title}'`}
      >
        {apiError}
        <Layout>
          <Layout.AnnotatedSection
            title="Discount details"
            description="Enter the percentage discount to be applied once the customer guesses the right secret code"
          >
            <Form onSubmit={() => handleSubmit()}>
              <Card sectioned>
                <FormLayout>
                  <FormLayout.Group>
                    <TextField
                      id="discount"
                      value={discount}
                      label="Discount"
                      type="number"
                      prefix="%"
                      step={1}
                      max={10}
                      onChange={handleDiscountChange}
                      placeholder="10"
                      error={discountError}
                    />
                    <TextField
                      id="max_tries"
                      value={maxTries}
                      label="Max Tries"
                      type="number"
                      step={1}
                      onChange={handleMaxTriesChange}
                      placeholder="5"
                      error={maxTriesError}
                    />
                    <p>
                      This is the discount percentage that will
                       be applied when the code is guessed correctly
                    </p>
                    <p>
                      This is the maximum amount of guesses each customer has for this product
                    </p>
                  </FormLayout.Group>
                  <FormLayout.Group>
                    <TextField
                      id="secret_code"
                      value={secretCode}
                      label="Secret Code"
                      type="number"
                      step={1}
                      min={1}
                      max={1000}
                      onChange={handleSecretCodeChange}
                      error={secretCodeError}
                    />
                  </FormLayout.Group>
                  <FormLayout.Group>
                    <p>
                      This is the secret code that customers will need to guess
                      The higher the number the harder it will be, adjust the max tries
                      for large numbers.
                    </p>
                  </FormLayout.Group>
                  <FormLayout.Group>
                    <Button onClick={handleGenerate}>Generate new secret code</Button>
                  </FormLayout.Group>
                  <FormLayout.Group>
                    <SettingToggle
                      action={{
                        content: isEnabledText,
                        onAction: handleIsEnabledChange,
                      }}
                      enabled={isEnabled}
                    >
                      {isEnabledText} the guessing game for this product
                    </SettingToggle>
                  </FormLayout.Group>
                </FormLayout>
              </Card>
            </Form>
          </Layout.AnnotatedSection>
        </Layout>
      </Page>
      {toastMarkup}
    </Frame>
  );
}
