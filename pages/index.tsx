import React, { useCallback, useState } from 'react';
import gql from 'graphql-tag';
import { EmptyState, Layout, Page, Frame, TopBar, InlineError } from '@shopify/polaris';
import { Loading, ResourcePicker, TitleBar } from '@shopify/app-bridge-react';
import store from 'store-js';
import ResourceListWithProducts from '../components/ResourceList';
import { useQuery } from '@apollo/react-hooks';
import { META_NAMESPACE } from '../server/constants';

const img = 'https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg';

// Get products and private meta fields
// Set up to use pagination via the cursor
// For now just shows the first 50
const GET_PRODUCTS = gql`
  query getProducts($cursor: String) {
    products(first: 50, after: $cursor) {
      edges {
        cursor
        node {
          id
          title
          descriptionHtml
          handle
          images(first: 1) {
            edges {
              node {
                originalSrc
                altText
              }
            }
          }
          privateMetafields(namespace: "${META_NAMESPACE}", first: 10) {
            edges {
              node {
                id
                namespace
                key
                value
              }
            }
          }
        }
      }
    }
  }
`;

export default function Index() {
  const [cursor, setCursor] = useState(undefined);
  const [started, setStarted] = useState(false);
  const { loading, error, data } = useQuery(GET_PRODUCTS, {
    variables: { cursor },
  });

  if (loading) {
    return (
      <Frame>
        <Loading />
      </Frame>
    );
  }
  if (error) {
    return (
      <Frame>
        <InlineError message="An error occurred" fieldID="none" />
      </Frame>
    );
  }

  // Check to see if any products have u-reckon meta fields
  // If not show the landing page
  const showEmptyState = data.products?.edges?.find(
    p => p.node?.privateMetafields?.edges?.length > 0,
  ) === undefined && !started;

  return showEmptyState ? (
    <Page title="You reckon?">
      <Layout>
        <EmptyState
          heading="Keep your customers guessing"
          action={{
            content: 'Get Started!',
            onAction: () => { setStarted(true); },
          }}
          image={img}
        >
          <p>
            You reckon is a guessing game that you can add to your product page,
            if your customer gets it right, they get a discount code!
            </p>
        </EmptyState>
      </Layout>
    </Page>
  ) : (
    <Frame
      topBar={<TopBar />}
    >
      <Page title="You reckon? - Select a product">
        <ResourceListWithProducts items={data.products.edges} />
      </Page>
    </Frame>
  );
}
