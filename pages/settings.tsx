import React, { useCallback, useState } from 'react';
import {
  Button,
  Card,
  Form,
  FormLayout,
  Frame,
  InlineError,
  Layout,
  Page,
  SettingToggle,
  Stack,
  TextField,
  TextStyle,
} from '@shopify/polaris';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { ENABLED_FOR_SHOP, META_NAMESPACE } from '../server/constants';
import gql from 'graphql-tag';
import { Loading } from '@shopify/app-bridge-react';
import SettingsForm from '../components/SettingsForm';

// Query to return shop and it's private metafields
const SHOP_METAFIELD_QUERY = gql`
  query {
    shop {
      id
      privateMetafields(namespace: "${META_NAMESPACE}", first: 10) {
        edges {
          node {
            id
            namespace
            key
            value
          }
        }
      }
    }
  }
`;

export default function Settings() {
  const { loading, error, data } = useQuery(SHOP_METAFIELD_QUERY);

  if (loading) {
    return (
      <Frame>
        <Loading />
      </Frame>
    );
  }
  if (error) {
    return (
      <Frame>
        <InlineError message="An error occurred" fieldID="none" />
      </Frame>
    );
  }

  return (
    <Page>
      <SettingsForm shop={data.shop} />
    </Page>
  );
}
