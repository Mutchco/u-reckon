// Metafield namespace
export const META_NAMESPACE = 'u-reckon';
// Shop metafield keys
export const ENABLED_FOR_SHOP = 'u-reckon-shop-enabled';
export const MAX_DISCOUNT_CODES = 'u-reckon-max-discounts';

// Product metafield keys
export const DISCOUNT = 'u-reckon-discount';
export const MAX_TRIES = 'u-reckon-max-tries';
export const SECRET_CODE = 'u-reckon-secret-code';
export const ENABLED_FOR_PRODUCT = 'u-reckon-is-enabled';
export const PRICE_RULE_ID = 'u-reckon-pricerule-id';
