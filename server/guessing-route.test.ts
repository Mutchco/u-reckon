import { unpackToken, validateCanGuess } from './guessing-route';

it('Should return object from token unpack', () => {
  const payload = unpackToken('');

  expect(payload).toEqual({
    products: {},
    discounts: {},
    receivedDiscounts: 0,
  });
});

it('Can guess should be true', () => {
  const payload = {
    discounts: {},
    products: {},
    receivedDiscounts: 0,
  };

  const [canGuess, message] = validateCanGuess(
    5,
    5,
    'handle',
    payload,
  );

  expect(canGuess).toEqual(true);
  expect(message).toEqual('');
});

it('Can guess should be false because of maxTries', () => {
  const handle = 'product-handle';
  const payload = {
    products: {
      [handle]: 11,
    },
    discounts: {},
    receivedDiscounts: 0,
  };

  const [canGuess, message] = validateCanGuess(
    10,
    1,
    handle,
    payload,
  );

  expect(canGuess).toEqual(false);
  expect(message).toContain('guesses');
});

it('Can guess should be false because already discounted product', () => {
  const handle = 'product-handle';
  const payload = {
    products: {},
    discounts: {
      [handle]: 1,
    },
    receivedDiscounts: 0,
  };

  const [canGuess, message] = validateCanGuess(
    10,
    1,
    handle,
    payload,
  );

  expect(canGuess).toEqual(false);
  expect(message).toContain('product');
});

it('Can guess should be false because already discounted product', () => {
  const handle = 'product-handle';
  const payload = {
    products: {},
    discounts: {},
    receivedDiscounts: 1,
  };

  const [canGuess, message] = validateCanGuess(
    10,
    1,
    handle,
    payload,
  );

  expect(canGuess).toEqual(false);
  expect(message).toContain('codes');
});
