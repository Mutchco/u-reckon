import { Context, Next } from 'koa';
import Shopify from 'shopify-api-node';
import { ACTIVE_SHOPIFY_SHOPS } from './server';
import jwt from 'jsonwebtoken';
import randomWord from 'random-word';
import {
  DISCOUNT,
  ENABLED_FOR_PRODUCT,
  ENABLED_FOR_SHOP,
  MAX_DISCOUNT_CODES,
  MAX_TRIES,
  META_NAMESPACE,
  PRICE_RULE_ID,
  SECRET_CODE,
} from './constants';

type RequestBody = {
  shop: string,
  productHandle: string,
  token: string,
  guess: string,
};

type TokenPayload = {
  products: { [index:string]: number },
  discounts: { [index:string]: number },
  receivedDiscounts: number,
};

type ResponseData = {
  isCorrect: boolean,
  isHigher?: boolean,
  discountCode?: string,
  guessesLeft?: number,
  productId: string,
};

type ResponseDisable = {
  isDisabled: boolean,
};

type ResponseBody = {
  token: string,
  message: string,
  success: boolean,
  data: ResponseData | ResponseDisable,
};

const A_VERY_VERY_VERY_SECRET_SECRET = 'shh_its_a_secret';

const tokensDb = new Map();

// Main koa route for the external api
// TODO: rate limiting, ip blocking
// TODO: Error handling
export default async function guessingRoute(ctx: Context, next: Next) {
  const { ip } = ctx;
  const { shop, productHandle, guess, token }: RequestBody = ctx.request.body;
  const { accessToken } = ACTIVE_SHOPIFY_SHOPS[shop];

  const jot = token || tokensDb.get(ip);

  const shopify = new Shopify({
    accessToken,
    shopName: shop,
  });

  // Query shop metafields
  const { shop: shopResult } = await shopify.graphql(SHOP_METAFIELD_QUERY);
  const storeMeta = shopResult.privateMetafields.edges.reduce(
    reducePrivateMetafields,
    {},
  );

  // Query product metafields
  const { productByHandle } = await shopify.graphql(PRODUCT_METAFIELD_QUERY, {
    handle: productHandle,
  });

  const productMeta = productByHandle.privateMetafields.edges.reduce(
    reducePrivateMetafields,
    {},
  );

  // If either the product or shop level disabled is set
  // return a message and a flag to ensure the html is hidden client-side
  if (
    productMeta[ENABLED_FOR_SHOP] === 'disabled'
    || productMeta[ENABLED_FOR_PRODUCT] === 'disabled'
  ) {
    const response: ResponseBody = {
      token,
      success: false,
      message: 'The guessing game has been disabled by the shop owner, sorry!',
      data: {
        isDisabled: true,
      },
    };
    ctx.response.body = JSON.stringify(response);
    return next();
  }

  const numberGuess = parseFloat(guess);
  // Parsing out all the numeric values from the store and product metafields
  const maxDiscounts = parseFloat(storeMeta[MAX_DISCOUNT_CODES] || '1');
  const secretCode = parseFloat(productMeta[SECRET_CODE]);
  const discount = parseFloat(productMeta[DISCOUNT]);
  const maxTries = parseFloat(productMeta[MAX_TRIES]);

  // Unpack the token or create new payload
  const payload: TokenPayload = unpackToken(jot);

  if (!payload.products[productHandle]) {
    payload.products[productHandle] = 0;
  }

  // Validate if the user can guess
  // TODO: Add ip validation
  const [canGuess, message] = validateCanGuess(
    maxTries,
    maxDiscounts,
    productHandle,
    payload,
  );

  // Response data object
  const data: ResponseData = {
    isCorrect: false,
    productId: productByHandle.variants?.edges[0]?.node?.id,
  };

  if (canGuess) {
    if (numberGuess === secretCode) {
      // Yay you guessed it
      data.isCorrect = true;
      const code = `${randomWord()}-${randomWord()}`;
      // Add a new discount code to the price rule
      const res = await shopify.graphql(DISCOUNT_CODE_MUTATION, {
        code,
        priceRuleId: productMeta[PRICE_RULE_ID],
      });
      // Use res.userErrors here to return fail
      if (res.userErrors) {
        ctx.response.body = JSON.stringify({
          success: false,
          message: 'Could not save discount code',
          data: {},
        });
        return next();
      }

      payload.discounts[productHandle] = 1;
      payload.receivedDiscounts += 1;
      data.discountCode = code;
    } else if (!isNaN(numberGuess)) {
      // Increment attempts
      payload.products[productHandle] += 1;
      data.isHigher = numberGuess > secretCode;
    }

    data.guessesLeft = maxTries - payload.products[productHandle];
  }

  // Create new token from update values
  const newToken = jwt.sign(
    payload,
    A_VERY_VERY_VERY_SECRET_SECRET,
  );

  // Add the new token to the in memory store
  tokensDb.set(ip, newToken);

  // Separate object for type safety
  const response: ResponseBody = {
    message,
    data,
    token: newToken,
    success: canGuess,
  };

  ctx.response.body = JSON.stringify(response);
  await next();
}

const reducePrivateMetafields = (acc: any, curr: any) => {
  const { key, value } = curr.node;
  acc[key] = value;
  return acc;
};

export const validateCanGuess = (
  maxTries: number,
  maxDiscounts: number,
  productHandle: string,
  payload: TokenPayload,
): [boolean, string] => {
  if (payload.discounts[productHandle] >= 1) {
    return [false, 'Sorry, you have already received a discount for this product'];
  }

  if (payload.receivedDiscounts >= maxDiscounts) {
    return [false, 'Sorry, you have received the max amount of discount codes'];
  }

  if (payload.products[productHandle] >= maxTries) {
    return [false, 'Sorry, you have used up all your guesses!'];
  }

  return [true, ''];
};

// Unpack jwt token
// If there's no token, it's the first time we've seen this customer
// and we'll need to construct the payload
export const unpackToken = (token?: string) => {
  return !token
    ?  {
      products: {},
      discounts: {},
      receivedDiscounts: 0,
    }
    : jwt.verify(token, A_VERY_VERY_VERY_SECRET_SECRET) as TokenPayload;
};

// Query to return product and it's private metafields
const PRODUCT_METAFIELD_QUERY = `
  query ($handle: String!) {
    productByHandle(handle: $handle) {
      id
      handle
      variants(first:10) {
        edges {
          node {
            id
          }
        }
      }
      privateMetafields(namespace: "${META_NAMESPACE}", first: 10) {
        edges {
          node {
            id
            namespace
            key
            value
          }
        }
      }
    }
  }
`;

// Query to return shop and it's private metafields
const SHOP_METAFIELD_QUERY = `
  query {
    shop {
      id
      privateMetafields(namespace: "${META_NAMESPACE}", first: 10) {
        edges {
          node {
            id
            namespace
            key
            value
          }
        }
      }
    }
  }
`;

// Mutation to add a price rule
const DISCOUNT_CODE_MUTATION = `
  mutation priceRuleDiscountCodeCreate($priceRuleId: ID!, $code: String!) {
    priceRuleDiscountCodeCreate(priceRuleId: $priceRuleId, code: $code) {
      priceRule {
        id
      }
      priceRuleDiscountCode {
        id
      }
      priceRuleUserErrors {
        code
        field
        message
      }
    }
  }
`;
