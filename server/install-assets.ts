import { readFile } from 'fs-extra';
const SCRIPT_NAME = 'ureckon.js';
const SNIPPET_NAME = 'snippets/ureckon-snippet.liquid';
const PRODUCT_FORM = 'snippets/product-form.liquid';

type ScriptResponse = { id: number, src: string, event: string };
type ScriptListResponse = {
  script_tags: ScriptResponse[],
};

type ThemeResponse = { id: number, name: string, role: string };
type ThemeListResponse = {
  themes: ThemeResponse[],
};

type AssetResponse = { asset: { key: string, value: string } };
type AssetListResponse = {
  assets: AssetResponse[],
};

// Main asset installation function
export default async function installAssets(
  accessToken: string,
  shop: string,
) {
  const API_HOST = process.env.HOST;
  const makeRequest = async function send<T>(
    endpoint: string,
    method: string = 'GET',
    body?: any,
  ): Promise<T> {
    try {
      const opts: { method: string, headers: any, body?: string } = {
        method,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'X-Shopify-Access-Token': accessToken,
        },
      };

      if (body !== undefined) {
        opts.body = JSON.stringify(body);
      }

      const resp = await fetch(`https://${shop}${endpoint}`, opts);

      const result: T = await resp.json();

      return result as T;
    } catch (e) {
      console.error(e);
      throw e;
    }
  };

  const scriptTags = await makeRequest<ScriptListResponse>(
    `/admin/api/2021-01/script_tags.json?src=${API_HOST}/${SCRIPT_NAME}`,
  );

  if (scriptTags.script_tags.length === 0) {
    await makeRequest<ScriptResponse>('/admin/api/2021-01/script_tags.json', 'POST', {
      script_tag: {
        event: 'onload',
        src: `${API_HOST}/${SCRIPT_NAME}`,
      },
    });
  }

  const themes = await makeRequest<ThemeListResponse>('/admin/api/2021-01/themes.json');

  const activeTheme = themes.themes.find(t => t.role === 'main');

  if (!activeTheme) {
    throw new Error('No theme?');
  }

  const snippetFile = await readFile(`${process.cwd()}/theme-assets/ureckon-snippet.liquid`);

  const snippet = await makeRequest<AssetResponse>(
    `/admin/api/2021-01/themes/${activeTheme.id}/assets.json`,
    'PUT',
    {
      asset: {
        key: SNIPPET_NAME,
        value: snippetFile.toString(),
      },
    },
  );
}
